let Currency = require('../models/currency');
let Inventory = require('../models/inventory');
let Register = require('../models/register');
let Display = require('../models/display');

let currency = new Currency().Currency;
let inventory = new Inventory().Inventory;
let register = new Register().Register;
let display = new Display().Display;

let events = require('events');
let MachineListener = new events.EventEmitter();

MachineListener.addListener('insertCurrency', register.insertCurrency);
MachineListener.addListener('selectItem', inventory.selectItem);
MachineListener.addListener('selectCoinReturn', register.returnCoins);

class Machine {
  constructor() {
    this.listener = MachineListener;
    this.currency = currency;
    this.inventory = inventory;
    this.register = register;
    this.display = display;
    display.refreshDisplay('INSERT COIN');
  }
}

module.exports = Machine;
