let Display = require('../models/display');
let display = new Display().Display;

let Register = require('../models/register');
let register = new Register().Register;


let InventoryObj = {
  currentInventory: {
    'cola': {price: 100, available: 10},
    'chips': {price: 50, available: 2},
    'candy': {price: 65, available: 0}
  },
  _selectedItems: [],
  _recentlyDispensed: false,
  getAvailableInventory: function(name) {
    if(InventoryObj.currentInventory[name]) {
      if(InventoryObj.currentInventory[name].available >= 1) {
        return InventoryObj.currentInventory[name].available;
      }
    }
    return 'SOLD OUT';
  },
  getItemPrice: function(name) {
    if(InventoryObj.currentInventory[name]) {
      return InventoryObj.currentInventory[name].price;
    }
    return 'SOLD OUT';
  },
  incrementAvailableInventory: function(name) {
    if(InventoryObj.currentInventory[name]) {
      InventoryObj.currentInventory[name].available++;
    }
  },
  decrementAvailableInventory: function(name) {
    if(InventoryObj.currentInventory[name]) {
      InventoryObj.currentInventory[name].available--;
    }
  },
  dispense: function(selectedItem) {
    display.refreshDisplay('THANK YOU');
    InventoryObj.decrementAvailableInventory(selectedItem);
    InventoryObj._recentlyDispensed = true;
  },
  selectItem: function(selectedItem) {
    // This function has grown and should be broken up
    //TODO: refactor function
    InventoryObj._selectedItems.push(selectedItem);

    if( InventoryObj._recentlyDispensed ) {
      display.refreshDisplay('INSERT COIN');
      InventoryObj._recentlyDispensed = false;
    }

    if( InventoryObj._inItemDebugMenu() ) {
      display.refreshDisplay('ENTERING DEBUG...');
    }

    let price = InventoryObj.getItemPrice(selectedItem);
    let canAfford = register.userCanAfford(price);
    let insertedCash = register.getInsertedCashValue()
    let supplyAvailable = InventoryObj.getAvailableInventory(selectedItem);
    let soldout = false;

    if(supplyAvailable == 0 || supplyAvailable == 'SOLD OUT') {
      soldout = true;
    }
    let selectedItemLength = InventoryObj._selectedItems.length;
    if( supplyAvailable >= 1 && canAfford) {
      change = register.canMakeChange(price, register.registerCash.bankedCash);
      if (register.canMakeChange(price, register.registerCash.bankedCash)) {
            if( change != -1 ) {
              register.dispenseChange(change);
              register.insertCurrencyIntoBank(register.registerCash.insertedCash);
              InventoryObj.dispense(selectedItem);
            } else {
              display.refreshDisplay('EXACT CHANGE ONLY');
            }
      }
    } else if (supplyAvailable >= 1 && !canAfford && insertedCash == 0) {
      display.refreshDisplay('INSERT COIN');
    } else if (supplyAvailable >= 1 && !canAfford && insertedCash > 0) {
      display.refreshDisplay(register.getInsertedCashValue());
    } else if (supplyAvailable >= 1 && !canAfford && selectedItemLength == 1) {
      display.refreshDisplay('PRICE');
      setTimeout(() => {
        display.refreshDisplay(price);
      }, 1500);
    } else if (soldout && selectedItemLength >= 2) {
      display.refreshDisplay(register.getInsertedCashValue());
    } else if (soldout) {
      display.refreshDisplay('SOLD OUT');
    } else {

    }
  },
  _inItemDebugMenu: function() {
    let match = 0;
    let debugMenuOrder = ['cola', 'chips', 'candy', 'cola', 'cola'];
    debugMenuOrder.forEach((debugItem, indx) => {
        if( debugItem == InventoryObj._selectedItems[indx] ) {
          match += 1;
        } else {
          match -= 1;
        }
    });
    if( match == debugMenuOrder.length ) {
      return true;
    }
    return false;
  }
}

class Inventory {
  constructor() {
    this.Inventory = InventoryObj;
  }
}

module.exports = Inventory;
