let Currency = require('../models/currency');
let currency = new Currency().Currency;

let Display = require('../models/display');
let display = new Display().Display;

let RegisterObj = {
  registerCash: {
    'insertedCash': [],
    'bankedCash': []
  },
  _haveAvailableCoinsForChange: function(cashNeeded, bankedCash) {
    socanMakeChange = true;
    for (var i = 0; i < cashNeeded.length; i++) {
      if (bankedCash.indexOf(cashNeeded[i]) == -1) {
        socanMakeChange = false;
      }
    }
    if (socanMakeChange) {
      return cashNeeded;
    } else {
      return -1;
    }
  },
  canMakeChange: function(price, bankedCash) {
      let cashValue = RegisterObj.getInsertedCashValue();
      let changeGoal = cashValue - price;
      if ( changeGoal == 0) {
        return true;
      }
      let cashUsed = [];
      let minCash = [];

      for (var i = 0; i < changeGoal+1; i++) {
        cashUsed.push(0);
        minCash.push(0);
      }
      for(let cents=0; cents <= changeGoal+1; cents++){
        let centsTemp = cents;
        let newCoin = 1;
        let cashValStore = [];
        for(let cashPiece=0; cashPiece < bankedCash.length; cashPiece++){
          let cashVal = bankedCash[cashPiece];
          if( cashVal <= cents ){
            cashValStore.push(cashVal);
          }
        }

        for(let indx=0; indx< cashValStore.length; indx++){
          let cashVal = cashValStore[indx];
          if ((minCash[cents-cashVal] + 1) < centsTemp ) {
            centsTemp = minCash[cents-cashVal]+1;
            newCoin = cashVal;
          }
        }

        minCash[cents] = centsTemp;
        cashUsed[cents] = newCoin;
      }

      cashNeeded = [];
      changeLeft = changeGoal;
      while (changeLeft > 0){
        thisCoin = cashUsed[changeLeft];
        cashNeeded.push(thisCoin);
        changeLeft = changeLeft - thisCoin;
      }
      return RegisterObj._haveAvailableCoinsForChange(cashNeeded, bankedCash)
  },
  userCanAfford: function(price) {
    if (price <= RegisterObj.getInsertedCashValue()) {
      return true;
    } else {
      return false;
    }
  },
  getInsertedCashValue: function() {
    let currentValue = 0;
    for (let cash of RegisterObj.registerCash.insertedCash) {
      cash = currency.determinCurrency(cash);
      currentValue += currency.getCurrencyValue(cash);
    }
    return currentValue;
  },
  getInsertedBankedValue: function() {
    let currentValue = 0;
    for (let cash of RegisterObj.registerCash.bankedCash) {
      currentValue += currency.getCurrencyValue(cash);
    }
    return currentValue;
  },
  insertCurrency: function(insertedCurrency) {
    if (insertedCurrency != 'invalid') {
      RegisterObj.registerCash.insertedCash.push(insertedCurrency);
    } else {
      eventEmitter.emit('returnCoins', 'last_coin');
    }
  },
  insertCurrencyIntoBank: function(insertedCurrency) {
    let bankedCash = RegisterObj.registerCash.bankedCash;
    let insertedCash = RegisterObj.registerCash.insertedCash;

    bankedCash = bankedCash.concat(insertedCash);
    RegisterObj.registerCash.bankedCash = bankedCash;
    RegisterObj.registerCash.insertedCash = [];

  },
  getInsertedCurrencyValue: function() {
    let currentValue = 0;
    RegisterObj.registerCash.insertedCash.forEach((cash) => {
      currentValue += currency.getCurrencyValue(cash);
    });
    return currentValue;
  },
  dispenseChange: function( ) {
    display.refreshDisplay('INSERT COIN');
  },
  returnCoins: function(last_coin) {
    if(last_coin) {
        RegisterObj.registerCash.insertedCash.pop();
    } else {
      RegisterObj.registerCash.insertedCash = [];
    }
    display.refreshDisplay('INSERT COIN');
  }
}

class Register {
  constructor() {
    this.Register = RegisterObj;
  }
}

module.exports = Register;
