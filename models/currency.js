const CurrencyObj = {
  currencies: [
    // { name:'penny',value: 1, weight: 2.5, thickness: 1.52, diameter: 19.05 },
    { name:'nickel',value: 5, weight: 5, thickness: 1.95, diameter: 21.21 },
    { name:'dime',value: 10, weight: 2.268, thickness: 1.35, diameter: 17.91 },
    { name:'quarter',value: 25, weight: 5.67, thickness: 1.75, diameter: 24.26 }
  ],
  // Cheating a little here with the currencyObject
  // Assume we have weight, thickness, and diameter from machine internals
  determinCurrency: function(currencyObject) {
    currencies = this.currencies.filter(element =>
        element.weight === currencyObject.weight);

    currencies = this.currencies.filter(element =>
        element.thickness === currencyObject.thickness);

    currencies = this.currencies.filter(element =>
        element.diameter === currencyObject.diameter);
    // Keep the machine simple, we ONLY care if we find something we know about
    if( currencies.length == 1 ) {
      return currencies[0].name;
    } else if ( currencies.length >= 2 ) {
      // should never happen based on current dataset.
      return 'invalid';
    } else {
      return 'invalid';
    }
  },
  getCurrencyValue: function(currencyName){
    currencyValue = this.currencies.filter(element =>
        element.name === currencyName);
    if (currencyValue.length == 1) {
      return currencyValue[0].value;
    } else {
      return 0;
    }
  }
}

class Currency {
  constructor() {
    this.Currency = CurrencyObj;
  }
}

module.exports = Currency;
