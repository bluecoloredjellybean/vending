let assert = require('assert');
let Inventory = require('../models/inventory');
let inventory = new Inventory().Inventory;

describe('inventory', function() {
  beforeEach(function(done) {
    inventory.currentInventory = {
        'cola': {price: 100, available: 10},
        'chips': {price: 50, available: 2},
        'candy': {price: 65, available: 0}
      };
      inventory._selectedItems = [];
    return done();
  });
  describe('getAvailableInventory', function() {
    it('should return "SOLD OUT" when available inventory is 0', function() {
      // NOTE: This test assumes a test model ( candy in our case )
      let itemName = 'candy';
      assert.equal('SOLD OUT', inventory.getAvailableInventory(itemName));
    });
    it('should return "SOLD OUT" when non-valid name is provided', function() {
      // NOTE: This test assumes a test model ( candy in this case )
      let itemName = 'fake item';
      assert.equal('SOLD OUT', inventory.getAvailableInventory(itemName));
    });
    it('should return "2" when available inventory is 2', function() {
      // NOTE: This test assumes a test model ( chips in this case )
      let itemName = 'chips';
      assert.equal(2, inventory.getAvailableInventory(itemName));
    });
  });
  describe('getItemPrice', function() {
    it('should return "SOLD OUT" when item is not defined', function() {
      // NOTE: This test assumes a test model ( candy in our case )
      let itemName = 'scotch';
      assert.equal('SOLD OUT', inventory.getItemPrice(itemName));
    });
    it('should return "1" when item is not defined', function() {
      // NOTE: This test assumes a test model ( candy in our case )
      let itemName = 'candy';
      assert.equal(65, inventory.getItemPrice(itemName));
    });
  });
  describe('incrementAvailableInventory', function() {
    it('should increment the available inventory for an item', function() {
      let itemName = 'chips';
      assert.equal(2, inventory.getAvailableInventory(itemName));
      inventory.incrementAvailableInventory(itemName);
      assert.equal(3, inventory.getAvailableInventory(itemName));
    });
  });
  describe('decrementAvailableInventory', function() {
    it('should decrement the available inventory for an item', function() {
      let itemName = 'chips';
      assert.equal(2, inventory.getAvailableInventory(itemName));
      inventory.decrementAvailableInventory(itemName);
      assert.equal(1, inventory.getAvailableInventory(itemName));
    });
  });
  describe('_inItemDebugMenu', function() {
    it('should return false when the _selectedItems is incorrect', function() {
      inventory.selectItem('chips');
      assert.equal(false, inventory._inItemDebugMenu());
    });
    it('should return true when _selectedItems stack is inserted', function() {
      inventory.selectItem('cola');
      inventory.selectItem('chips');
      inventory.selectItem('candy');
      inventory.selectItem('cola');
      inventory.selectItem('cola');
      assert.equal(true, inventory._inItemDebugMenu());
    });
  });
});
