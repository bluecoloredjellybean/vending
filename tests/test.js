let assert = require('assert');
let Machine = require('../controllers/machine');
let Inventory = require('../models/inventory');
let inventory = new Inventory().Inventory;
let Register = require('../models/register');
let register = new Register().Register;

// simulate user inputs
let nickel = {weight: 5, thickness: 1.95, diameter: 21.21};
let dime = {weight: 2.268, thickness: 1.35, diameter: 17.91};
let quarter = {weight: 5.67, thickness: 1.75, diameter: 24.26};

// inital 'test' test case to ensure mocha properly installed
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal(-1, [1,2,3].indexOf(4));
    });
  });
});


describe('VendingMachine', function() {
  beforeEach(function(done) {
    inventory.currentInventory = {
        'cola': {price: 100, available: 10},
        'chips': {price: 50, available: 2},
        'candy': {price: 65, available: 0}
      };
      inventory._selectedItems = [];
    return done();
  });
  beforeEach(function(done) {
    register.registerCash = {
      'insertedCash': [],
      'bankedCash': []
    }
    return done();
  });
  describe('User purchasing a chips with exact change', function() {

    it('should return true when change is is possible', function() {
      let machine = new Machine();

      assert.equal(0,register.getInsertedBankedValue());
      assert.equal(2,inventory.getAvailableInventory('chips'));
      machine.listener.emit('selectItem', 'chips');
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('insertCurrency', dime);
      machine.listener.emit('item_selection', 'chips');
      machine.listener.emit('insertCurrency', dime);
      machine.listener.emit('insertCurrency', nickel);
      machine.listener.emit('selectItem', 'chips');
      assert.equal(1,inventory.getAvailableInventory('chips'));
      assert.equal(0,register.getInsertedBankedValue());
    });

    it('should display EXACT CHANGE ONLY', function() {
      let machine = new Machine();
      register.registerCash = {
        'insertedCash': [],
        'bankedCash': [25]
      };
      inventory.currentInventory.candy.available = 1;
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('selectItem', 'candy');
      assert.equal('EXACT CHANGE ONLY', machine.display.message);
    });


  });
  describe('An item being sold out', function() {
    it('should display the message SOLD OUT', function() {
      let machine = new Machine();
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('selectItem', 'candy');
      assert.equal('SOLD OUT',machine.display.message);
    });
    it('should display remaining money in machine', function() {
      let machine = new Machine();
      machine.listener.emit('insertCurrency', quarter);
      machine.listener.emit('selectItem', 'candy');
      machine.listener.emit('selectItem', 'candy');
      assert.equal(25,machine.display.message);
    });
  });
  describe('User using the coin return', function() {
    xit('should have emptied insertedCash when pressed', function() {
      let machine = new Machine();
      assert.equal(0,register.getInsertedCashValue());

      machine.listener.emit('insertCurrency', quarter);
      assert.equal(25,machine.register.getInsertedCashValue());
      machine.listener.emit('returnCoins');
      assert.equal(10,machine.register.getInsertedCashValue());
      assert.equal(true,true);
    });
  });

});
