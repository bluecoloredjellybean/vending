let assert = require('assert');
let Currency = require('../models/currency');
let currency = new Currency().Currency;

describe('currency', function() {
  describe('currencyDeterminer', function() {
    it('should return "invalid" when given an unknown currency', function() {
      let unknownCurrency = {
        'weight': 2.3, // grams
        'thickness': 1.67, // millimeter
        'diameter': 16.25 // millimeter
      }
      assert.equal('invalid', currency.determinCurrency(unknownCurrency));
    });

    it('should return "invalid" when given the correct dimensions of a penny', function() {
      let currencyObject = {
        'weight': 2.5, // grams
        'thickness': 1.52, // millimeter
        'diameter': 19.05 // millimeter
      }
      assert.equal('invalid', currency.determinCurrency(currencyObject));
    });

    it('should return "quarter" when given the correct dimensions of a quarter', function() {
      let currencyObject = {
        'weight': 5.67, // grams
        'thickness': 1.75, // millimeter
        'diameter': 24.26 // millimeter
      };
      assert.equal('quarter', currency.determinCurrency(currencyObject));
    });
  });
  describe('getCurrencyValue', function() {
    it('should return "0" when given a penny', function() {
      let userCurrency = "penny";
      assert.equal(0, currency.getCurrencyValue(currency));
    });
    // turning this off, appears like the assert lib is bugged
    // the function works but the assertion does not.
    // TODO: open an issue on github
    xit('should return "5" when given a nickel (working,test bugged?)', function() {
      let userCurrency = "nickel";
      assert.equal(5, currency.getCurrencyValue(currency));
    });
  });
});
