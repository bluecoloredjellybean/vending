let assert = require('assert');
let Register = require('../models/register');
let register = new Register().Register;


describe('register', function() {
  beforeEach(function(done) {
    register.registerCash = {
      'insertedCash': [],
      'bankedCash': []
    }
    return done();
  });
  describe('getInsertedCashValue', function() {
    it('should return 0 when user has not inserted any cash', function() {
      assert.equal(0, register.getInsertedCashValue());
    });
  });
  describe('getInsertedBankedValue', function() {
    it('should return 0 when the bank is empty', function() {
      assert.equal(0, register.getInsertedBankedValue());
    });
  });
  describe('getInsertedBankedValue', function() {
    it('should return 0 when the bank is empty', function() {
      register.insertCurrency('nickel')
      register.insertCurrencyIntoBank()
      assert.equal(5, register.getInsertedBankedValue());
    });
  });

  describe('getInsertedCurrencyValue', function() {
    it('should return 0 when the bank is empty', function() {
      assert.equal(0, register.getInsertedCurrencyValue());
    });
  });

  describe('getInsertedCurrencyValue', function() {
    it('should return 5 when the bank as an inserted nickel', function() {
      assert.equal(0, register.getInsertedCurrencyValue());
      register.insertCurrency('nickel')
      assert.equal(5, register.getInsertedCurrencyValue());
    });
  });

  describe('insertCurrency', function() {
    it('should update the array when valid coin inserted', function() {
      register.insertCurrency('nickel')
      assert.equal(5, register.getInsertedCurrencyValue());
    });
  });

  describe('returnCoins', function() {
    it('should empty the insertedCurrency field', function() {
      register.insertCurrency('nickel')
      assert.equal(5, register.getInsertedCurrencyValue());
      register.returnCoins();
      assert.equal([], register.getInsertedCurrencyValue());
    });
  });
});
